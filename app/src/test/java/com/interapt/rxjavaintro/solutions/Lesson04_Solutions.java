package com.interapt.rxjavaintro.solutions;

import org.junit.Test;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.observables.GroupedObservable;
import rx.observables.MathObservable;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Holden Easley <heasley@interapthq.com>
 * @version 1
 * @since 2/2/17
 */
public class Lesson04_Solutions {

  public String received = "";

  private String evenNums = "";
  private String oddNums = "";

  /*
  So far everything has been pretty linear. Our pipelines all took the form:
  "do this, then do this, then do this, then end". In reality we can combine pipelines. We can take two streams
  and turn them into a single stream.
  Now its worth nothing this is different from what we did when we nested Observables. In that case we always had one stream.
  Lets take a stream of integers and a stream of strings and join them.
  */
  @Test public void _1_merging() {
    Observable<Object> you = Observable.just(1, 2, 3);
    Observable<String> me = Observable.just("A", "B", "C");

    you.mergeWith(me).subscribe(new Action1<Object>() {
      @Override public void call(Object s) {
        received += s + " ";
      }
    });

    assertThat(received).isEqualTo("1 2 3 A B C ");
  }

  /*
  We can also split up a single stream into two streams. We are going to to use the groupBy() action.
  This action can be a little tricky because it emits an observable of observables. So we need to subscribe to the
  "parent" observable and each emitted observable.
  We encourage you to read more from the wiki: http://reactivex.io/documentation/operators/groupby.html
  Lets split up a single stream of integers into two streams: even and odd numbers.
  */
  @Test public void _2_splittingUp() {
    Observable.range(1, 9).groupBy(new Func1<Integer, String>() {
      @Override public String call(Integer integer) {
        if (integer % 2 == 0) {
          return "even";
        } else {
          return "odd";
        }
      }
    }).subscribe(new Action1<GroupedObservable<String, Integer>>() {
      @Override public void call(final GroupedObservable<String, Integer> group) {
        group.subscribe(new Action1<Integer>() {
          @Override public void call(Integer integer) {
            String key = group.getKey();
            if ("even".equals(key)) {
              evenNums = evenNums + integer;
            } else if ("odd".equals(key)) {
              oddNums = oddNums + integer;
            }
          }
        });
      }
    });

    assertThat(evenNums).isEqualTo("2468");
    assertThat(oddNums).isEqualTo("13579");
  }

  /*
  Lets take what we know now and do some cool stuff. We've setup an observable and a function for you. Lets combine
  them together to average some numbers.
  Also see that we need to subscribe first to the "parent" observable but that the pipeline still cold until we
  subscribe to each subset observable. Don't forget to do that.
   */
  @Test public void _3_challenge_needToSubscribeImmediatelyWhenSplitting() {
    final double[] averages = { 0, 0 };
    Observable<Integer> numbers = Observable.just(22, 22, 99, 22, 101, 22);
    Func1<Integer, Integer> keySelector = new Func1<Integer, Integer>() {
      @Override public Integer call(Integer integer) {
        return integer % 2;
      }
    };
    Observable<GroupedObservable<Integer, Integer>> split = numbers.groupBy(keySelector);
    split.subscribe(new Action1<GroupedObservable<Integer, Integer>>() {
      @Override public void call(final GroupedObservable<Integer, Integer> group) {
        Observable<Double> convertToDouble = group.map(new Func1<Integer, Double>() {
          @Override public Double call(Integer integer) {
            return (double) integer;
          }
        });
        Func1<Double, Double> insertIntoAveragesArray = new Func1<Double, Double>() {
          @Override public Double call(Double aDouble) {
            return averages[group.getKey()] = aDouble;
          }
        };
        MathObservable.averageDouble(convertToDouble).map(insertIntoAveragesArray).subscribe();
      }
    });

    assertThat(averages[0]).isEqualTo(22.0);
    assertThat(averages[1]).isEqualTo(100.0);
  }
}
