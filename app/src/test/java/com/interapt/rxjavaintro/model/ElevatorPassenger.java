package com.interapt.rxjavaintro.model;

/**
 * @author Holden Easley <heasley@interapthq.com>
 * @version 1
 * @since 2/2/17
 */
public class ElevatorPassenger {

  public int weightInPounds;
  private String name;

  public ElevatorPassenger(String name, int weightInPounds) {
    this.name = name;
    this.weightInPounds = weightInPounds;
  }

  public int getWeightInPounds() {
    return weightInPounds;
  }

  public String getName() {
    return name;
  }

  public String toString() {
    return "ElevatorPassenger{" +
        "name='" + name + '\'' +
        ", weightInPounds=" + weightInPounds +
        '}';
  }
}
