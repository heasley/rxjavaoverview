package com.interapt.rxjavaintro.model;

import java.util.ArrayList;
import java.util.List;
import rx.Observable;
import rx.functions.Func2;

/**
 * @author Holden Easley <heasley@interapthq.com>
 * @version 1
 * @since 2/2/17
 */
public class Elevator {

  public static final int MAX_CAPACITY_POUNDS = 500;
  List<ElevatorPassenger> passengers = new ArrayList<>();

  public void addPassenger(ElevatorPassenger passenger) {
    passengers.add(passenger);
  }

  public int getTotalWeightInPounds() {
    return Observable.from(passengers).reduce(0, new Func2<Integer, ElevatorPassenger, Integer>() {
      @Override
      public Integer call(Integer accumulatedWeight, ElevatorPassenger elevatorPassenger) {
        return elevatorPassenger.getWeightInPounds() + accumulatedWeight;
      }
    }).toBlocking().last();
  }

  public List<ElevatorPassenger> getPassengers() {
    return passengers;
  }

  public int getPassengerCount() {
    return passengers.size();
  }

  @Override public String toString() {
    return "Elevator{" +
        "passengers=" + passengers + "\n" +
        "totalWeight=" + getTotalWeightInPounds() +
        '}';
  }

  public void unload() {
    passengers = new ArrayList<>();
  }
}
