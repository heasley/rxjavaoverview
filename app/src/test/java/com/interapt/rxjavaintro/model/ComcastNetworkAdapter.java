package com.interapt.rxjavaintro.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Holden Easley <heasley@interapthq.com>
 * @version 1
 * @since 2/2/17
 */
public class ComcastNetworkAdapter {
  private int attemptCount;

  public List<String> getData() {
    if (attemptCount < 42) {
      attemptCount++;
      System.out.println("network issues!! please reboot your computer!");
      return null;
    }
    ArrayList<String> data = new ArrayList<>();
    data.add("extremely important data");
    System.out.println("transmitting data!");
    return data;
  }
}
