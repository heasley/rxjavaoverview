package com.interapt.rxjavaintro.model;

/**
 * @author Holden Easley <heasley@interapthq.com>
 * @version 1
 * @since 2/2/17
 */
public class CarnivalFood {

  private String name;
  private Double price;

  public CarnivalFood(String name, Double price) {
    this.name = name;
    this.price = price;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  @Override public String toString() {
    return "Food{" +
        "name='" + name + '\'' +
        ", price=" + price +
        "\n}";
  }
}
